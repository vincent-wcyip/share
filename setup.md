# HKU Space Recommendation Engine Setup

## Prerequisite

### 1. Volume

The recommendation engine requires a data volume for storing analytics data for processing. The same volume will be mounted to various pods during job execution.

### 2. Network access to Redis DB on HKU SPACE Website Servers

The recommendation engine needs to access Redis DB HKU SPACE Website Servers. Please kindly ensure the network connectivity such as appropiate firewall configurations.

### 3. Helm

Helm is required to install the Kubernetes Operator for Apache Spark.

## Setup Procedures

### 1. Create namespace for Recommentation Engine Jobs

```bash
kubectl create namespace spacere
```

It is fine to use any existing namespace or create a namespace with name other than `spacere`. If a different namespace is to be used, please replace the namespace in all the following commands and yaml.

### 2. Install The Kubernetes Operator for Apache Spark

[The Kubernetes Operator for Apache Spark](https://github.com/GoogleCloudPlatform/spark-on-k8s-operator) is a 3rd-party tools for managing spark jobs on Kubernetes. To install the operator, use the Helm chart.

```bash
helm repo add spark-operator https://googlecloudplatform.github.io/spark-on-k8s-operator

helm install spacere spark-operator/spark-operator --namespace spark-operator --set sparkJobNamespace=spacere --set webhook.enable=true
```

A service account name, ending with `-spark` and starting with the Helm release name, and RBAC will be created in the `sparkJobNamespace`. In the above case, the service account name would be `spacere-spark`.

### 3. Create Service for Redis

```bash
kubectl apply -f service_redis.yaml --namespace=spacere
```

Please kindly replace the correct values when appropiate. Please note that the `service name` will be required in the configmap below. FQDN is preferred in the `ip` field below to ensure transparency during DB failover or migration.

> *service_redis.yaml*

```yaml
apiVersion: v1
kind: Service
metadata:
  name: redis-space
spec:
  ports:
    - protocol: TCP
      port: 6379
      targetPort: 6379
        
---
apiVersion: v1
kind: Endpoints
metadata:
  name: redis-space
subsets:
- addresses:
  - ip: 192.168.49.1
  ports:
  - port: 6379
```

### 4. Create Config map

```bash
kubectl apply -f configmap_spacere.yaml --namespace=spacere
```

Please kindly replace the correct values when appropiate.

> *configmap_spacere.yaml*

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: spacere
data:
  SPACERE_BASEDIR : ""
  GCP_SPACE_PROJECT_ID : "space-poc-331407"
  GCP_SPACE_DATASET_ID : "space-poc-331407.analytics_283576688"
  REDIS_HOST : "redis-space"
  REDIS_PORT : "6379"
  REDIS_DBNUM : "5"
```

### 5. Create Secrets

A. Google Cloud Platform Service Account Key

```bash
kubectl create secret generic gcp-space --from-file=key.json=space-poc-331407-8214e48fb493.json --namespace=spacere
```

B. Redis Secrets

```bash
kubectl apply -f secret_redis.yaml --namespace=spacere
```

Please replace the `password` below in `base64` encoding. To get base64 encoded text, you could use the following command:

```bash
echo -n 'password'|base64
```

> *secret_redis.yaml*

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: redis-space
type: Opaque
data:
  password: ""
```
